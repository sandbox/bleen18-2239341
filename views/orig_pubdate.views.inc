<?php

/**
 * Implements hook_views_data_alter().
 */
function orig_pubdate_views_data_alter(&$data) {
  $data['node']['originally_published'] = array(
      'title' => t('Original Publish Date'),
      'help' => t('The date the content was published for the first time.'),
    ) + $data['node']['created'];
}
