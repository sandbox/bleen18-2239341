<?php

/**
 * @file
 * Test file for Original Publish Date module.
 */

class OrigPubdateTestCase extends DrupalWebTestCase {
  /**
   * Implementation of setUp().
   */
  public function setUp() {
    // Enable a couple modules.
    parent::setUp('orig_pubdate');
    menu_rebuild();

    // Create an admin user with all the permissions needed to run tests.
    $this->admin_user = $this->drupalCreateUser(array('create article content', 'edit any page content', 'bypass node access', 'administer nodes'));
    $this->drupalLogin($this->admin_user);
  }

  public static function getInfo() {
    return array(
      'name' => 'Basic Functionality',
      'description' => 'Tests the basic functionality of Original Publish Date.',
      'group' => 'Original Publish Date',
    );
  }

  public function testOrigPubdate() {
    $originally_published = REQUEST_TIME;

    // Create a node and assert the original published date is correct.
    $node = $this->drupalCreateNode();
    $this->assertEqual($originally_published, $node->originally_published, 'The original publish date was correctly set.');

    // Unpublish the node and assert the original published date is still correct.
    $this->drupalPost('node/' . $node->nid . '/edit', array('status' => 0), t('Save'));
    $node = node_load($node->nid, NULL, TRUE);
    $this->assertEqual($originally_published, $node->originally_published, 'The original publish date was correct even after unpublishing.');

    // Re-publish the node and assert the original published date is still correct.
    $this->drupalPost('node/' . $node->nid . '/edit', array('status' => 1), t('Save'));
    $node = node_load($node->nid, NULL, TRUE);
    $this->assertEqual($originally_published, $node->originally_published, 'The original publish date was correct even after re-publishing.');
  }
}
